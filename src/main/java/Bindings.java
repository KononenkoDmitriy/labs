import Controller.StudentClass;
import Daos.Connection.HibernateSessionFactoryUtil;
import Daos.DaoAudiance;
import Daos.DaoGroup;
import Daos.DaoLesson;
import Daos.DaoStudent;
import Model.Audiance;
import Services.ServiceAudience;
import Services.ServiceGroup;
import Services.ServiceLesson;
import Services.ServiceStudent;
import View.View;
import com.google.inject.AbstractModule;

public class Bindings extends AbstractModule {
    @Override
    protected void configure() {
        bind(HibernateSessionFactoryUtil.class).to(HibernateSessionFactoryUtil.class).asEagerSingleton();
        bind(DaoAudiance.class).to(DaoAudiance.class);
        bind(ServiceAudience.class).to(ServiceAudience.class);

        bind(DaoGroup.class).to(DaoGroup.class);
        bind(ServiceGroup.class).to(ServiceGroup.class);

        bind(DaoStudent.class).to(DaoStudent.class);
        bind(ServiceStudent.class).to(ServiceStudent.class);
        bind(StudentClass.class).to(StudentClass.class);

        bind(DaoLesson.class).to(DaoLesson.class);
        bind(ServiceLesson.class).to(ServiceLesson.class);


    }
}
