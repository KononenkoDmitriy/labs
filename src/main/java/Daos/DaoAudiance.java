package Daos;

import Daos.Connection.HibernateSessionFactoryUtil;
import Model.Audiance;
import Model.Group;
import Model.Lesson;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
//import utils.HibernateSessionFactoryUtil;
import java.util.List;
import Model.Audiance;
public class DaoAudiance {
    public Audiance findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Audiance.class, id);
    }

    public void save(Audiance audiance) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(audiance);
        tx1.commit();
        session.close();
    }

    public void update(Audiance audiance) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(audiance);
        tx1.commit();
        session.close();
    }

    public void delete(Audiance audiance) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(audiance);
        tx1.commit();
        session.close();
    }
    public void insert(Audiance audiance) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(audiance);
        tx1.commit();
        session.close();
    }

    public Audiance findAutoById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Audiance.class, id);
    }

    public List<Audiance> findAll() {
            List<Audiance> audiance= (List<Audiance>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From" + Audiance.class.getSimpleName()).list();
            return audiance;
    }

}
