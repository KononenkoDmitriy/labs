package Daos;
import Daos.Connection.HibernateSessionFactoryUtil;


import Model.Lesson;
import Daos.Connection.HibernateSessionFactoryUtil;
import Model.Group;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
public class DaoLesson {
    public Lesson findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Lesson.class, id);
    }

    public void save(Lesson lesson) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(lesson);
        tx1.commit();
        session.close();
    }

    public void update(Lesson lesson) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(lesson);
        tx1.commit();
        session.close();
    }

    public void delete(Lesson lesson) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(lesson);
        tx1.commit();
        session.close();
    }
    public void insert(Lesson lesson) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(lesson);
        tx1.commit();
        session.close();
    }

    public Lesson findAutoById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Lesson.class, id);
    }

    public List<Lesson> findAll() {
        List<Lesson> lesson= (List<Lesson>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From" + Lesson.class.getSimpleName()).list();
        return lesson;
    }
    public int quantityHall(Lesson lesson){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("select quantityHall from  "+Lesson.class);
        Integer quantity = query.getFirstResult();
        return quantity;
    }

}
