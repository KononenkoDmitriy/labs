package Daos;
import Daos.Connection.HibernateSessionFactoryUtil;



import Model.Professor;
import Daos.Connection.HibernateSessionFactoryUtil;
import Model.Group;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;
public class DaoProfessor {
    public Professor findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Professor.class, id);
    }

    public void save(Professor professor) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(professor);
        tx1.commit();
        session.close();
    }

    public void update(Professor professor) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(professor);
        tx1.commit();
        session.close();
    }

    public void delete(Professor professor) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(professor);
        tx1.commit();
        session.close();
    }
    public void insert(Professor professor) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(professor);
        tx1.commit();
        session.close();
    }

    public Professor findAutoById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Professor.class, id);
    }

    public List<Professor> findAll() {
        List<Professor> professor= (List<Professor>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From" +Professor.class.getSimpleName()).list();
        return professor;
    }
}
