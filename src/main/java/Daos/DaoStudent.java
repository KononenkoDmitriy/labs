package Daos;
import Daos.Connection.HibernateSessionFactoryUtil;



import Model.Student;
import Daos.Connection.HibernateSessionFactoryUtil;
import Model.Group;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;
public class DaoStudent {

    public Student findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Student.class, id);
    }

    public void save(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(student);
        tx1.commit();
        session.close();
    }

    public void update(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(student);
        tx1.commit();
        session.close();
    }

    public void delete(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(student);
        tx1.commit();
        session.close();
    }
    public void insert(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(student);
        tx1.commit();
        session.close();
    }

    public Student findAutoById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Student.class, id);
    }

    public List<Student> findAll() {
        List<Student> student= (List<Student>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From" + Student.class.getSimpleName()).list();
        return student;
    }

}
