package Daos;
import Daos.Connection.HibernateSessionFactoryUtil;



import Model.Subject;
import Daos.Connection.HibernateSessionFactoryUtil;
import Model.Group;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;
public class DaoSubject {
    public Subject findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Subject.class, id);
    }

    public void save(Subject subject) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(subject);
        tx1.commit();
        session.close();
    }

    public void update(Subject subject) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(subject);
        tx1.commit();
        session.close();
    }

    public void delete(Subject subject) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(subject);
        tx1.commit();
        session.close();
    }
    public void insert(Subject subject) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(subject);
        tx1.commit();
        session.close();
    }

    public Subject findAutoById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Subject.class, id);
    }

    public List<Subject> findAll() {
        List<Subject> subject= (List<Subject>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From" + Subject.class.getSimpleName()).list();
        return subject;
    }
}

