package Model;
import javax.persistence.*;
import java.util.List;
@Entity
@Table (name = "Группа")
public class Group {

    private String name;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public int getQuantityGroup() {
        return quantityGroup;
    }

    public void setQuantityGroup(int quantityGroup) {
        this.quantityGroup = quantityGroup;
    }

    private int quantityGroup;
    @Column(name = "name")
    @OneToMany(mappedBy = "Группа", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Group> groupList;
    public Group(String name){
        this.name=name;
    }
    public Group(){

    }




    public void removeGroup(Group group){
        groupList.remove(group);
    }
    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

}
