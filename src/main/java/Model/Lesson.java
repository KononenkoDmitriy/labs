package Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Пара")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    public int idProfessor;
    public int nameAudiance;

    String time;
    String dayOfWeek;
    int idSubject;
    public int idGroup;
    private List<Lesson> lessonList;
    //private User user;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(int idProfessor) {
        this.idProfessor = idProfessor;
    }

    public int getIdHall() {
        return nameAudiance;
    }

    public void setIdHall(int idHall) {
        this.nameAudiance = idHall;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(int idSubject) {
        this.idSubject = idSubject;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public Lesson(int id, int idProfessor, int nameAudiance, String time, String dayOfWeek, int idSubject, int idGroup){
        this.id=id;
        this.idProfessor=idProfessor;
        this.nameAudiance=nameAudiance;
        this.time=time;
        this.dayOfWeek=dayOfWeek;
        this.idSubject=idSubject;
        this.idGroup=idGroup;
    }
}
