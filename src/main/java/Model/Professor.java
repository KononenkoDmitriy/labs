package Model;

import java.util.List;

public class Professor {
    public int getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(int idProfessor) {
        this.idProfessor = idProfessor;
    }

    public String getNameProfessor() {
        return nameProfessor;
    }

    public void setNameProfessor(String nameProfessor) {
        this.nameProfessor = nameProfessor;
    }

    public String getSurnameProfessor() {
        return surnameProfessor;
    }

    public void setSurnameProfessor(String surnameProfessor) {
        this.surnameProfessor = surnameProfessor;
    }

    public String getPatronymicsProfessor() {
        return patronymicsProfessor;
    }

    public void setPatronymicsProfessor(String patronymicsProfessor) {
        this.patronymicsProfessor = patronymicsProfessor;
    }

    private int idProfessor;
    private String nameProfessor;
    private String surnameProfessor;
    private String patronymicsProfessor;
    List<Professor> professorList;

    public Professor(int idProfessor,String nameProfessor,String surnameProfessor,String patronymicsProfessor){
        this.idProfessor=idProfessor;
        this.nameProfessor=nameProfessor;
        this.surnameProfessor=surnameProfessor;
        this.patronymicsProfessor=patronymicsProfessor;


    }
}
