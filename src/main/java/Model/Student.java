package Model;

import java.util.List;

public class Student {
    int idStudent;

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getSurnameStudent() {
        return surnameStudent;
    }

    public void setSurnameStudent(String surnameStudent) {
        this.surnameStudent = surnameStudent;
    }

    public String getPatronymicStudent() {
        return patronymicStudent;
    }

    public void setPatronymicStudent(String patronymicStudent) {
        this.patronymicStudent = patronymicStudent;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    private String surnameStudent;
    private String patronymicStudent;
    private String group;
    List<Student> studentList;

    public Student(int idStudent,String surnameStudent,String patronymicStudent,String group){
        this.idStudent=idStudent;
        this.patronymicStudent=patronymicStudent;
        this.surnameStudent=surnameStudent;
        this.group=group;
    }
}
