package Model;

import java.util.List;

public class Subject {
    public int getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(int idSubject) {
        this.idSubject = idSubject;
    }

    public String getNameSubject() {
        return nameSubject;
    }

    public void setNameSubject(String nameSubject) {
        this.nameSubject = nameSubject;
    }

    private int idSubject;
    private String nameSubject;
    List<Subject> subjectList;
    public Subject(int idSubject,String nameSubject){
        this.idSubject=idSubject;
        this.nameSubject=nameSubject;
    }
}
