package Services;

import Model.Audiance;

import java.util.List;

public interface IServiceAudience {
    public Audiance findAudience(int id)throws Exception;
    public void saveUser(Audiance audiance);
    public void deleteUser(Audiance audiance);
    public void updateUser(Audiance audiance);
    public List<Audiance> findAllUsers();
}
