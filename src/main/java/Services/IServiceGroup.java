package Services;

import Model.Group;

import java.util.List;

public interface IServiceGroup {
    public Group findGroup(int id)throws Exception;
    public void saveUser(Group group);
    public void deleteUser(Group group);
    public void updateUser(Group group);
    public List<Group> findAllUsers();
}
