package Services;

import Model.Lesson;

import java.util.List;

public interface IServiceLesson {
    public Lesson findGroup(int id)throws Exception;
    public void saveUser(Lesson lesson);
    public void deleteUser(Lesson lesson);
    public void updateUser(Lesson lesson);
    public List<Lesson> findAllUsers();

}
