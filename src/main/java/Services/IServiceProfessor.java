package Services;

import Model.Professor;

import java.util.List;

public interface IServiceProfessor {
    public Professor findGroup(int id)throws Exception;
    public void saveUser(Professor professor);
    public void deleteUser(Professor professor);
    public void updateUser(Professor professor);
    public List<Professor> findAllUsers();


}
