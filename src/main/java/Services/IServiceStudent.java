package Services;

import Model.Student;

import java.util.List;

public interface IServiceStudent {
    public Student findStudent(int id)throws Exception;
    public void saveUser(Student student);
    public void deleteUser(Student student);
    public void updateUser(Student student);
    public List<Student> findAllUsers();

}
