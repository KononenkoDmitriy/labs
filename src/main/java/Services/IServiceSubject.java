package Services;

import Model.Subject;

import java.util.List;

public interface IServiceSubject {
    public Subject findSubject(int id) throws Exception;
    public void saveUser(Subject subject);
    public void deleteUser(Subject subject);
    public void updateUser(Subject subject);
    public List<Subject> findAllUsers();
}
