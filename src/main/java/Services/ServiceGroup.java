package Services;


import Daos.DaoGroup;
import Model.Group;


import java.util.List;

public class ServiceGroup implements IServiceGroup {

    private DaoGroup daoGroup = new DaoGroup();

    public ServiceGroup() {
    }

    public Group findGroup(int id) throws Exception {
        try {
            return daoGroup.findById(id);
        }
        catch (Exception e){

              System.out.println("Такого айди не существует");
              return null;
        }

    }

    public void saveUser(Group group) {
        try {
            daoGroup.save(group);
        }
        catch(Exception  e){
            System.out.println("Не удалось сохранить группу");

        }
    }

    public void deleteUser(Group group) {
        daoGroup.delete(group);
    }

    public void updateUser(Group group) {
        daoGroup.update(group);
    }

    public List<Group> findAllUsers() {
        return daoGroup.findAll();
    }
}