package Services;


import Daos.Connection.HibernateSessionFactoryUtil;
import Daos.DaoGroup;
import Daos.DaoLesson;
import Model.Audiance;
import Model.Group;
import Model.Lesson;
import Daos.DaoAudiance;

import java.util.List;

public class ServiceLesson implements IServiceLesson {
    private DaoLesson daoLesson=new DaoLesson();
    private DaoGroup daoGroup=new DaoGroup();
    private DaoAudiance daoAudiance=new DaoAudiance();



    public ServiceLesson() throws Exception {
    }

    public Lesson findGroup(int id) {
        return daoLesson.findById(id);
    }

    public void saveUser(Lesson lesson)  {
        Audiance audiance=daoAudiance.findById(lesson.nameAudiance);
        Group group=daoGroup.findById(lesson.idGroup);

        if (audiance.countAudiance>=group.getQuantityGroup()){
            daoLesson.save(lesson);
        }
        else {
          System.out.println("Не хватает мест в аудитории");
        }
    }

    public void deleteUser(Lesson lesson) {
        daoLesson.delete(lesson);
    }

    public void updateUser(Lesson lesson) {
        daoLesson.update(lesson);
    }

    public List<Lesson> findAllUsers() {
        return daoLesson.findAll();
    }



}
