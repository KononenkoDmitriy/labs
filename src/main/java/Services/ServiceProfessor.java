package Services;





import Daos.DaoProfessor;
import Model.Professor;

import java.util.List;

public class ServiceProfessor implements  IServiceProfessor{
    private DaoProfessor daoProfessor=new DaoProfessor();



    public ServiceProfessor() {
    }

    public Professor findGroup(int id) {
        return daoProfessor.findById(id);
    }

    public void saveUser(Professor professor) {
        daoProfessor.save(professor);
    }


    public void deleteUser(Professor professor) {
        daoProfessor.delete(professor);
    }

    public void updateUser(Professor professor) {
        daoProfessor.update(professor);
    }

    public List<Professor> findAllUsers() {
        return daoProfessor.findAll();
    }
}
