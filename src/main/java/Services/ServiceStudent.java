package Services;



import Daos.DaoStudent;

import Model.Professor;
import Model.Student;

import java.util.List;

public class ServiceStudent implements IServiceStudent{
    private DaoStudent daoStudent=new DaoStudent();



    public ServiceStudent() {
    }

    public Student findStudent(int id) {
        return daoStudent.findById(id);
    }


    public void saveUser(Student student) {
        daoStudent.save(student);
    }

    public void deleteUser(Student student) {
        daoStudent.delete(student);
    }

    public void updateUser(Student student) {
        daoStudent.update(student);
    }

    public List<Student> findAllUsers() {
        return daoStudent.findAll();
    }
}
