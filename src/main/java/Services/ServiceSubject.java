package Services;



import Daos.DaoSubject;

import Model.Student;
import Model.Subject;

import java.util.List;

public class ServiceSubject implements IServiceSubject{
    private DaoSubject daoSubject=new DaoSubject();



    public ServiceSubject() {
    }

    public Subject findSubject(int id) {
        return daoSubject.findById(id);
    }

    public void saveUser(Subject subject) {
        daoSubject.save(subject);
    }

    public void deleteUser(Subject subject) {
        daoSubject.delete(subject);
    }

    public void updateUser(Subject subject) {
        daoSubject.update(subject);
    }

    public List<Subject> findAllUsers() {
        return daoSubject.findAll();
    }
}
