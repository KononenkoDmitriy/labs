package Services;

import Daos.DaoAudiance;
import Model.Audiance;


import java.util.List;

public class TestServiceAudience implements IServiceAudience  {
    private DaoAudiance daoAudience = new DaoAudiance();

    public TestServiceAudience() {
    }

    public Audiance findAudience(int id) throws Exception {
        try {
            return daoAudience.findById(id);
        }
        catch (Exception e){

            System.out.println("Такого айди не существует");
            return null;
        }

    }

    public void saveUser(Audiance audiance) {
        try {
            daoAudience.save(audiance);
        }
        catch(Exception  e){
            System.out.println("Не удалось сохранить аулиторию");

        }
    }

    public void deleteUser(Audiance audiance) {
        daoAudience.delete(audiance);
    }

    public void updateUser(Audiance audiance) {
        daoAudience.update(audiance);
    }

    public List<Audiance> findAllUsers() {
        return daoAudience.findAll();
    }
}
